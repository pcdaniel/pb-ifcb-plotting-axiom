## Power Buoy IFCB Plotting ##
---

First, sorry this code is pretty unstructured. I hadn't ever considered that it would be scaled so there is a lot of sloppy work.

I had to edit things a bit to get it work with old data. During deployments, I usually just have the script plot the past 7 days and then I shut it off if the instrument is down. Here I had to hotwire some things to get it to work with old data.

### Loading the Data ###

Data from the Power Buoy is stored in an appended .csv file. This data is a summary or the concentration (ie cells per mL) for each of the model classes for each syringe (ie .roi file). This is generated from taking the model output applying a threshold for the classification probability, then summing the number of hits in each class. Finally, the sample volume is calculated from the .adc file and used to divide each class sum into a cell per ml (ie concentration). As Kasia very correctly pointed out, there are some drawbacks to only considering cell concentration. This perspective largely undervalues chain-forming organisms, as they are counted as a single cell in this method. Biovolume is liekly a more appropriate metric to plot in that case.

---
